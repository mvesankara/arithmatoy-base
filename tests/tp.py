# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    if  n == 0:
        return "0"
    else:
        return "S" * n + "0"
    pass


def S(n: str) -> str:
    if n == '0':
        return "S0"
    else:
        return "S" + n 
    pass


def addition(a: str, b: str) -> str:
    if a != "0" and b != "0":
        return a[:-1] + b
    elif a == "0" and b != "0":
        return b
    elif a!= "0" and b == "0":
        return a
    else:
        return a + b

    pass


def multiplication(a: str, b: str) -> str:
    if a =='0' or b == '0':
        return '0'
    else:
        return a[:-1] * b[:-1] + '0'
    
    pass


def facto_ite(n: int) -> int:
    pass


def facto_rec(n: int) -> int:
    pass


def fibo_rec(n: int) -> int:
    pass


def fibo_ite(n: int) -> int:
    pass


def golden_phi(n: int) -> int:
    pass


def sqrt5(n: int) -> int:
    pass


def pow(a: float, n: int) -> float:
    pass
